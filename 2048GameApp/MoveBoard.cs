﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveBoard
     {
          public static void MakeMove(char direction,Hex[][] map, Player player)
          {
               if(direction == 'd')
               {
                    MoveHexRight.MoveRight(map);
                    if(CheckBoard.IfHexAdded(direction, map, player))
                    {
                         MoveHexRight.MoveRight(map);
                    }
               }
               else if (direction == 'x')
               {
                    MoveHexLowerRight.MoveLowerRight(map);
                    if (CheckBoard.IfHexAdded(direction, map, player))
                    {
                         MoveHexLowerRight.MoveLowerRight(map);
                    }
               }
               else if (direction == 'z')
               {
                    MoveHexLowerLeft.MoveLowerLeft(map);
                    if (CheckBoard.IfHexAdded(direction, map, player))
                    {
                         MoveHexLowerLeft.MoveLowerLeft(map);
                    }
               }
               else if (direction == 'a')
               {
                    MoveHexLeft.MoveLeft(map);
                    if (CheckBoard.IfHexAdded(direction,map, player))
                    {
                         MoveHexLeft.MoveLeft(map);
                    }
               }
               else if (direction == 'w')
               {
                    MoveHexUpperLeft.MoveUpperLeft(map);
                    if (CheckBoard.IfHexAdded(direction, map, player))
                    {
                         MoveHexUpperLeft.MoveUpperLeft(map);
                    }
               }
               else if (direction == 'e')
               {
                    MoveHexUpperRight.MoveUpperRight(map);
                    if (CheckBoard.IfHexAdded(direction, map, player))
                    {
                         MoveHexUpperRight.MoveUpperRight(map);
                    }
               }
          }

          public static bool AddHex(Hex[][] map,int i, int j,int moveI,int moveJ, Player player)
          {
               if((map[i][j].GetValue() != 0 || map[i+moveI][j+moveJ].GetValue() != 0) &&
                    map[i][j].GetValue() == map[i + moveI][j + moveJ].GetValue() &&
                    map[i][j].GetColor() == map[i + moveI][j + moveJ].GetColor())
               {
                    map[i][j].SetValue(map[i][j].GetValue() + map[i + moveI][j + moveJ].GetValue());
                    if(map[i][j].GetPlayer() == player.GetPlayerID())
                    {
                         player.SetScore(player.GetScore() + map[i][j].GetValue()/2);
                    }
                    map[i + moveI][j + moveJ].SetColor(0);
                    map[i + moveI][j + moveJ].SetPlayer(0);
                    map[i + moveI][j + moveJ].SetValue(0);
                    return true;
               }
               else
               {
                    return false;
               }
          }

          public static void ChangeBoard(Hex[][] map,int x,int y,int xChange,int yChange,int xsetNewHex, int ysetNewHex)
          {
               map[x + xChange][(y + yChange)].SetValue(map[x + xChange + ysetNewHex][y + yChange + xsetNewHex].GetValue());
               map[x + xChange][(y + yChange)].SetColor(map[x + xChange + ysetNewHex][y + yChange + xsetNewHex].GetColor());
               map[x + xChange][(y + yChange)].SetPlayer(map[x + xChange + ysetNewHex][y + yChange + xsetNewHex].GetPlayer());
               map[x + xChange + ysetNewHex][y + yChange + xsetNewHex].SetValue(0);
               map[x + xChange + ysetNewHex][y + yChange + xsetNewHex].SetColor(0);
               map[x + xChange + ysetNewHex][y + yChange + xsetNewHex].SetPlayer(0);
          }

     }
}
