﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class Hex
     {
          private int whichPlayer;
          private int playerColor;
          private int hexValue;
		public Hex(int player, int color, int value)
		{
			whichPlayer = player;
			playerColor = color;
			hexValue = value;
		}

		public void SetPlayer(int player)
          {
			whichPlayer = player;
		}

		public void SetColor(int color)
		{
			playerColor = color;
		}

		public void SetValue(int value)
		{
			hexValue = value;
		}

		public int GetPlayer()
		{
			return whichPlayer;
		}

		public int GetColor()
		{
			return playerColor;
		}

		public int GetValue()
		{
			return hexValue;
		}

	}
}
