﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{

     class Player
     {
          private int PlayerID;
          private int Score;

          public Player(int ID)
          {
               PlayerID = ID;
               Score = 0;
          }
          public void SetPlayerID(int ID)
          {
               PlayerID = ID;
          }
          public void SetScore(int score)
          {
               Score = score;
          }
          public int GetPlayerID()
          {
               return PlayerID;
          }
          public int GetScore()
          {
               return Score;
          }
     }
}
