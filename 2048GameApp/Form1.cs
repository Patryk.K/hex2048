﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2048GameApp
{
     public partial class Form1 : Form
     {
          public Form1()
          {
               InitializeComponent();
               initializeColorArrays();
          }

          private void startGame_Click(object sender, EventArgs e)
          {
               hexagonMap.SetRandomHexOnBoard(whoseTurn);
               drawBoard.Refresh();
               startGame.Enabled = false;
               howManyHex.Enabled = false;
               Right.Enabled = true;
               Left.Enabled = true;
               upperLeft.Enabled = true;
               upperRight.Enabled = true;
               lowerLeft.Enabled = true;
               lowerRight.Enabled = true;
            if (whoseTurn == 1) turnIndicator.Image = Properties.Resources.hexBlueTurn;
            else turnIndicator.Image = Properties.Resources.hexRedTurn;
          }

          private void howManyHex_ValueChanged(object sender, EventArgs e)
          {
            hexagonMap.ResizeMap(howManyHex.Value);
            hexPositions.Clear();
            textPosition.Clear();
            drawBoard.Refresh();
          }

          private void restart_Click(object sender, EventArgs e)
          {
               hexagonMap.ResizeMap(howManyHex.Value);
               drawBoard.Refresh();
               startGame.Enabled = true;
               howManyHex.Enabled = true;
               Right.Enabled = false;
               Left.Enabled = false;
               upperLeft.Enabled = false;
               upperRight.Enabled = false;
               lowerLeft.Enabled = false;
               lowerRight.Enabled = false;
               playerOne.SetScore(0);
               playerTwo.SetScore(0);
               turnIndicator.Image = Properties.Resources.hex;
          }

          private void Right_Click(object sender, EventArgs e)
          {
               MoveBoard.MakeMove('d',hexagonMap.map, ChoosePlayer(playerOne,playerTwo));
               ChangePlayer();
          }

          private void left_Click(object sender, EventArgs e)
          {
               MoveBoard.MakeMove('a', hexagonMap.map, ChoosePlayer(playerOne, playerTwo));
               ChangePlayer();
          }
          private void upperLeft_Click(object sender, EventArgs e)
          {
               MoveBoard.MakeMove('w', hexagonMap.map, ChoosePlayer(playerOne, playerTwo));
               ChangePlayer();
          }

          private void upperRight_Click(object sender, EventArgs e)
          {
               MoveBoard.MakeMove('e', hexagonMap.map, ChoosePlayer(playerOne, playerTwo));
               ChangePlayer();
          }

          private void lowerLeft_Click(object sender, EventArgs e)
          {
               MoveBoard.MakeMove('z', hexagonMap.map, ChoosePlayer(playerOne, playerTwo));
               ChangePlayer();
          }

          private void lowerRight_Click(object sender, EventArgs e)
          {
               MoveBoard.MakeMove('x', hexagonMap.map, ChoosePlayer(playerOne, playerTwo));
               ChangePlayer();
          }

          private Player ChoosePlayer(Player one, Player two)
          {
               if (whoseTurn == 1)
               {
                    return one;
               }
               else
               {
                    return two;
               }
          }

          private void ChangePlayer()
          {
               if (whoseTurn == 1)
               {
                    whoseTurn = 2;
                    turnIndicator.Image = Properties.Resources.hexRedTurn;
                }
               else
               {
                    whoseTurn = 1;
                    turnIndicator.Image = Properties.Resources.hexBlueTurn;
               }
               hexagonMap.SetRandomHexOnBoard(whoseTurn);
               drawBoard.Refresh();
          }
     }
}
