﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class CheckBoard
     {
          public static bool IfHexAdded(char direction, Hex[][] map, Player player)
          {
               bool wasAdded = false;
               if (direction == 'd')
               {
                    for(int i = 0; i<map.Length;i++)
                    {
                         for(int j = map[i].Length - 1; j > 0;j--)
                         {
                              if(MoveBoard.AddHex(map,i,j,0,-1, player))
                              {
                                   wasAdded = true;
                              }
                              else
                              {
                                   continue;
                              }
                         }
                    }
               }
               else if (direction == 'x')
               {
                    for (int i = map.Length-1; i > 0; i--)
                    {
                         for (int j = map[i].Length - 1; j >= 0; j--)
                         {
                              if (i > map[0].Length - 1)
                              {
                                   if (MoveBoard.AddHex(map, i, j, -1, 0, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                              if(i < map[0].Length && j > 0)
                              {
                                   if (MoveBoard.AddHex(map, i, j, -1, -1, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                         }
                    }
               }
               else if (direction == 'z')
               {
                    for (int i = map.Length - 1; i > 0; i--)
                    {
                         for (int j = map[i].Length - 1; j >= 0; j--)
                         {
                              if (i > map[0].Length - 1)
                              {
                                   if (MoveBoard.AddHex(map, i, j, -1,1, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                              if (i < map[0].Length && j < map[i-1].Length)
                              {
                                   if (MoveBoard.AddHex(map, i, j, -1,0, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                         }
                    }
               }
               else if (direction == 'a')
               {
                    for (int i = 0; i < map.Length; i++)
                    {
                         for (int j = 0; j < map[i].Length - 1; j++)
                         {
                              if (MoveBoard.AddHex(map, i, j, 0, 1, player))
                              {
                                   wasAdded = true;
                              }
                              else
                              {
                                   continue;
                              }
                         }
                    }
               }
               else if (direction == 'w')
               {
                    for (int i = 0; i < map.Length - 1; i++)
                    {
                         for (int j = 0; j < map[i].Length; j++)
                         {
                              if (i < map[0].Length - 1 && j + 1 <= map[i].Length)
                              {
                                   if (MoveBoard.AddHex(map, i, j, 1,1, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                              if (i > map[0].Length - 2 && j < map[i+1].Length)
                              {
                                   if (MoveBoard.AddHex(map, i, j, 1,0, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                         }
                    }
               }
               else if (direction == 'e')
               {
                    for (int i = 0; i < map.Length - 1; i++)
                    {
                         for (int j = 0; j < map[i].Length; j++)
                         {
                              if (i < map[0].Length - 1)
                              {
                                   if (MoveBoard.AddHex(map, i, j, 1,0, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                              if (i > map[0].Length - 2 && j > 0)
                              {
                                   if (MoveBoard.AddHex(map, i, j, 1, -1, player))
                                   {
                                        wasAdded = true;
                                   }
                                   else
                                   {
                                        continue;
                                   }
                              }
                         }
                    }
               }
               return wasAdded;
          }
     }
}
