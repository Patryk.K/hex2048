﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveHexUpperLeft
     {
          public static void MoveUpperLeft(Hex[][] map)
          {
               for (int i = 0; i < map.Length; i++)
               {
                    for (int j = 0; j < map[i].Length; j++)
                    {
                         int iterate = 1;
                         TopHalf(map, iterate, i, j);
                         BottomHalf(map, iterate, i, j);
                    }
               }
          }

          public static void TopHalf(Hex[][] map, int iterate, int i, int j)
          {
               if (i - iterate >= 0 && j - iterate >= 0)
               {
                    if (map[i][j].GetValue() != 0 && i < map[0].Length)
                    {
                         while (map[i - iterate][j - iterate].GetValue() == 0)
                         {
                              MoveBoard.ChangeBoard(map, i, j, -iterate, -iterate, 1, 1);
                              if (i - iterate > 0)
                              {
                                   iterate++;
                                   if (j - iterate < 0 || i - iterate < 0)
                                   {
                                        break;
                                   }
                              }
                         }
                    }
               }
          }
          public static void BottomHalf(Hex[][] map, int iterate, int i, int j)
          {
               if (map[i][j].GetValue() != 0 && i > map[0].Length - 1)
               {
                    while (map[i - iterate][j].GetValue() == 0)
                    {
                         MoveBoard.ChangeBoard(map, i, j, -iterate, 0, 0, 1);
                         if (i - iterate > map[0].Length - 1)
                         {
                              iterate++;
                              if (i - iterate < map[0].Length - 1)
                              {
                                   break;
                              }
                         }
                    }
                    int nextJ = 1;
                    iterate++;
                    TopHalfAfterBottom(map, iterate, nextJ, i, j);
               }
          }
          public static void TopHalfAfterBottom(Hex[][] map, int iterate, int nextJ, int i, int j)
          {
               if (i - iterate >= 0 && j - nextJ >= 0)
               {
                    while (map[i - iterate][j - nextJ].GetValue() == 0)
                    {
                         MoveBoard.ChangeBoard(map, i, j, -iterate, -nextJ, 1, 1);
                         if (i - iterate > 0)
                         {
                              iterate++;
                              nextJ++;
                              if (i - iterate < 0 || j - nextJ < 0)
                              {
                                   break;
                              }
                         }
                    }
               }
          }

     }
}
