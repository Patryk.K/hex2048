﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveHexLowerLeft
     {
          public static void MoveLowerLeft(Hex[][] map)
          {
               for (int i = map.Length - 1; i >= 0; i--)
               {
                    for (int j = map[i].Length - 1; j >= 0; j--)
                    {
                         int iterate = 1;
                         BottomHalf(map, iterate, i, j);
                         TopHalf(map, iterate, i, j);
                    }
               }
          }
          public static void BottomHalf(Hex[][] map,int iterate, int i, int j)
          {
               if (i + iterate < map.Length && j - iterate >= 0)
               {
                    if (map[i][j].GetValue() != 0 && i > map[0].Length - 2)
                    {
                         while (map[i + iterate][j - iterate].GetValue() == 0)
                         {
                              MoveBoard.ChangeBoard(map, i, j, iterate, -iterate, 1, -1);
                              if (i + iterate < map.Length)
                              {
                                   iterate++;
                                   if (j - iterate < 0 || i + iterate >= map.Length)
                                   {
                                        break;
                                   }
                              }
                         }
                    }
               }
          }
          public static void TopHalf(Hex[][] map, int iterate, int i, int j)
          {
               if (map[i][j].GetValue() != 0 && i < map[0].Length - 1)
               {
                    while (map[i + iterate][j].GetValue() == 0)
                    {
                         MoveBoard.ChangeBoard(map, i, j, iterate, 0, 0, -1);
                         if (i + iterate <= map[0].Length - 1)
                         {
                              iterate++;
                              if (i + iterate > map[0].Length - 1)
                              {
                                   break;
                              }
                         }
                    }
                    int nextJ = 1;
                    BottomHalfAfterTop(map, iterate, nextJ, i, j);
               }
          }
          public static void BottomHalfAfterTop(Hex[][] map, int iterate,int nextJ, int i, int j)
          {
               if (i + iterate < map.Length && j - nextJ >= 0)
               {
                    while (map[i + iterate][j - nextJ].GetValue() == 0)
                    {
                         MoveBoard.ChangeBoard(map, i, j, iterate, -nextJ, 1, -1);
                         if (i + iterate < map.Length)
                         {
                              iterate++;
                              nextJ++;
                              if (i + iterate > map.Length - 1 || j - nextJ < 0)
                              {
                                   break;
                              }
                         }
                    }
               }
          }
     }
}
