﻿using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;

namespace _2048GameApp

{
     partial class Form1
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
        private System.ComponentModel.IContainer components = null;
        private HexagonsMap hexagonMap = new HexagonsMap(5);
        private Player playerOne = new Player(1);
        private Player playerTwo = new Player(2);
        private int whoseTurn = 1;
        //for speeding up a bit the drawing of hexes, still slow anyway
        private Dictionary<Tuple<int, int, int>, PointF> hexPositions = new Dictionary<Tuple<int, int, int>, PointF>();
        private Dictionary<Tuple<int, int>, float> textPosition = new Dictionary<Tuple<int, int>, float>();
        private Dictionary<int, Color> blueLogColor = new Dictionary<int, Color>();
        private Dictionary<int, Color> redLogColor = new Dictionary<int, Color>();
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
            this.Points1 = new System.Windows.Forms.Label();
            this.startGame = new System.Windows.Forms.Button();
            this.restart = new System.Windows.Forms.Button();
            this.drawBoard = new System.Windows.Forms.Panel();
            this.howManyHex = new System.Windows.Forms.TrackBar();
            this.upperLeft = new System.Windows.Forms.Button();
            this.turnIndicator = new System.Windows.Forms.PictureBox();
            this.upperRight = new System.Windows.Forms.Button();
            this.Right = new System.Windows.Forms.Button();
            this.lowerRight = new System.Windows.Forms.Button();
            this.lowerLeft = new System.Windows.Forms.Button();
            this.Left = new System.Windows.Forms.Button();
            this.Points2 = new System.Windows.Forms.Label();
            this.Turn = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.howManyHex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turnIndicator)).BeginInit();
            this.SuspendLayout();
            // 
            // Points1
            // 
            this.Points1.AutoSize = true;
            this.Points1.Location = new System.Drawing.Point(12, 9);
            this.Points1.Name = "Points1";
            this.Points1.Size = new System.Drawing.Size(82, 13);
            this.Points1.TabIndex = 0;
            this.Points1.Text = "Points player 1: ";
            // 
            // startGame
            // 
            this.startGame.Location = new System.Drawing.Point(13, 593);
            this.startGame.Name = "startGame";
            this.startGame.Size = new System.Drawing.Size(75, 23);
            this.startGame.TabIndex = 1;
            this.startGame.Text = "Start Game";
            this.startGame.UseVisualStyleBackColor = true;
            this.startGame.Click += new System.EventHandler(this.startGame_Click);
            // 
            // restart
            // 
            this.restart.Location = new System.Drawing.Point(95, 592);
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(75, 23);
            this.restart.TabIndex = 2;
            this.restart.Text = "Restart";
            this.restart.UseVisualStyleBackColor = true;
            this.restart.Click += new System.EventHandler(this.restart_Click);
            // 
            // drawBoard
            // 
            this.drawBoard.Location = new System.Drawing.Point(178, 13);
            this.drawBoard.Name = "drawBoard";
            this.drawBoard.Size = new System.Drawing.Size(600, 600);
            this.drawBoard.TabIndex = 3;
            this.drawBoard.Paint += new System.Windows.Forms.PaintEventHandler(this.drawBoard_Paint);
            // 
            // howManyHex
            // 
            this.howManyHex.Location = new System.Drawing.Point(15, 541);
            this.howManyHex.Minimum = 3;
            this.howManyHex.Name = "howManyHex";
            this.howManyHex.Size = new System.Drawing.Size(157, 45);
            this.howManyHex.TabIndex = 4;
            this.howManyHex.Value = 5;
            this.howManyHex.ValueChanged += new System.EventHandler(this.howManyHex_ValueChanged);
            // 
            // upperLeft
            // 
            this.upperLeft.BackgroundImage = global::_2048GameApp.Properties.Resources.gornalewo;
            this.upperLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.upperLeft.Enabled = false;
            this.upperLeft.Location = new System.Drawing.Point(25, 368);
            this.upperLeft.Name = "upperLeft";
            this.upperLeft.Size = new System.Drawing.Size(49, 49);
            this.upperLeft.TabIndex = 5;
            this.upperLeft.UseVisualStyleBackColor = true;
            this.upperLeft.Click += new System.EventHandler(this.upperLeft_Click);
            // 
            // turnIndicator
            // 
            this.turnIndicator.Image = global::_2048GameApp.Properties.Resources.hex;
            this.turnIndicator.Location = new System.Drawing.Point(49, 408);
            this.turnIndicator.Name = "turnIndicator";
            this.turnIndicator.Size = new System.Drawing.Size(74, 76);
            this.turnIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.turnIndicator.TabIndex = 0;
            this.turnIndicator.TabStop = false;
            // 
            // upperRight
            // 
            this.upperRight.BackgroundImage = global::_2048GameApp.Properties.Resources.gornaprawo;
            this.upperRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.upperRight.Enabled = false;
            this.upperRight.Location = new System.Drawing.Point(99, 368);
            this.upperRight.Name = "upperRight";
            this.upperRight.Size = new System.Drawing.Size(48, 49);
            this.upperRight.TabIndex = 6;
            this.upperRight.UseVisualStyleBackColor = true;
            this.upperRight.Click += new System.EventHandler(this.upperRight_Click);
            // 
            // Right
            // 
            this.Right.BackgroundImage = global::_2048GameApp.Properties.Resources.prawo;
            this.Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Right.Enabled = false;
            this.Right.Location = new System.Drawing.Point(122, 423);
            this.Right.Name = "Right";
            this.Right.Size = new System.Drawing.Size(48, 49);
            this.Right.TabIndex = 7;
            this.Right.UseVisualStyleBackColor = true;
            this.Right.Click += new System.EventHandler(this.Right_Click);
            // 
            // lowerRight
            // 
            this.lowerRight.BackgroundImage = global::_2048GameApp.Properties.Resources.dolprawo;
            this.lowerRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lowerRight.Enabled = false;
            this.lowerRight.Location = new System.Drawing.Point(99, 478);
            this.lowerRight.Name = "lowerRight";
            this.lowerRight.Size = new System.Drawing.Size(48, 50);
            this.lowerRight.TabIndex = 8;
            this.lowerRight.UseVisualStyleBackColor = true;
            this.lowerRight.Click += new System.EventHandler(this.lowerRight_Click);
            // 
            // lowerLeft
            // 
            this.lowerLeft.BackgroundImage = global::_2048GameApp.Properties.Resources.dollewo;
            this.lowerLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lowerLeft.Enabled = false;
            this.lowerLeft.Location = new System.Drawing.Point(25, 478);
            this.lowerLeft.Name = "lowerLeft";
            this.lowerLeft.Size = new System.Drawing.Size(49, 50);
            this.lowerLeft.TabIndex = 9;
            this.lowerLeft.UseVisualStyleBackColor = true;
            this.lowerLeft.Click += new System.EventHandler(this.lowerLeft_Click);
            // 
            // Left
            // 
            this.Left.BackgroundImage = global::_2048GameApp.Properties.Resources.lewo;
            this.Left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Left.Enabled = false;
            this.Left.Location = new System.Drawing.Point(5, 423);
            this.Left.Name = "Left";
            this.Left.Size = new System.Drawing.Size(45, 49);
            this.Left.TabIndex = 10;
            this.Left.UseVisualStyleBackColor = true;
            this.Left.Click += new System.EventHandler(this.left_Click);
            // 
            // Points2
            // 
            this.Points2.AutoSize = true;
            this.Points2.Location = new System.Drawing.Point(12, 32);
            this.Points2.Name = "Points2";
            this.Points2.Size = new System.Drawing.Size(82, 13);
            this.Points2.TabIndex = 11;
            this.Points2.Text = "Points player 2: ";
            // 
            // Turn
            // 
            this.Turn.AutoSize = true;
            this.Turn.Location = new System.Drawing.Point(12, 55);
            this.Turn.Name = "Turn";
            this.Turn.Size = new System.Drawing.Size(78, 13);
            this.Turn.TabIndex = 12;
            this.Turn.Text = "Turn of player: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 620);
            this.Controls.Add(this.Turn);
            this.Controls.Add(this.Points2);
            this.Controls.Add(this.Right);
            this.Controls.Add(this.lowerRight);
            this.Controls.Add(this.Left);
            this.Controls.Add(this.lowerLeft);
            this.Controls.Add(this.upperRight);
            this.Controls.Add(this.upperLeft);
            this.Controls.Add(this.turnIndicator);
            this.Controls.Add(this.howManyHex);
            this.Controls.Add(this.drawBoard);
            this.Controls.Add(this.restart);
            this.Controls.Add(this.startGame);
            this.Controls.Add(this.Points1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.howManyHex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turnIndicator)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

          }
        private void initializeColorArrays()
        {
            for(int i = 1; i<16; i++)
            {
                blueLogColor.Add((int)Math.Pow(2,i),Color.FromArgb(1 * (100 + (10 * (int)Math.Log((int)Math.Pow(2, i), 2)) % 155), 0 * 255, 0 * 255, 1 * 255));
                redLogColor.Add((int)Math.Pow(2, i), Color.FromArgb(1 * (100 + (10 * (int)Math.Log((int)Math.Pow(2, i), 2)) % 155), 1 * 255, 0 * 255, 0 * 255));
            }
        }

          private void drawBoard_Paint(object sender, PaintEventArgs e)
          {
               var graphics = e.Graphics;
               int fontSize = 10;

               SolidBrush blueBrush = new SolidBrush(Color.Blue); 
               Font textFont = new Font("Arial", fontSize);
               SolidBrush textBrush = new SolidBrush(Color.Black);

               var shape = new PointF[6];
               int angle = 60;
               int angleRotation = 90;
               float radius = drawBoard.Width/(hexagonMap.map.Length + hexagonMap.map.Length-1);
               int moveHexagons = 0;
               float x_0 = (NumericExtensions.GetXdistance(radius) / 2)* (hexagonMap.map.Length/2 + 1) + 5*howManyHex.Value;
               float y_0 = (drawBoard.Height - 2*radius*(hexagonMap.map[0].Length) - (radius/2)*(hexagonMap.map[0].Length - 1))/2 - 2 * (howManyHex.Value-1);
            for (int i = 0; i < hexagonMap.map.Length; i++)
               {
                    if (i >= howManyHex.Value)
                    {
                         moveHexagons++;
                    }
                    for (int j = 0; j < hexagonMap.map[i].Length; j++)
                    {

                         DrawHexagon(graphics, shape, ref angle, ref angleRotation, ref radius, ref moveHexagons, ref x_0, ref y_0, ref i, ref j);
                         DrawHexValue(graphics, textFont, textBrush, i, j, radius, x_0, y_0, x_0, y_0, angle, moveHexagons, fontSize, angleRotation);
                    }
               }
        }
          private void DrawHexagon(Graphics graphics, PointF[] shape,ref int angle, ref int angleRotation, ref float radius,
                                  ref int moveHexagons, ref float startPointX, ref float startPointY, ref int moveHexInX, ref int moveHexInY)
          {
           
            for (int a = 0; a < 6; a++)
               {
                    Tuple<int, int, int> tuple = new Tuple<int, int, int>(moveHexInX, moveHexInY, a);
                    if (hexPositions.ContainsKey(tuple))
                    {
                        shape[a] = hexPositions[tuple];
                    }
                    else
                    {
                        if (moveHexInX < howManyHex.Value)
                        {
                            shape[a] = NumericExtensions.calcHexagonPointUpper(angle, angleRotation, radius, moveHexagons, startPointX, startPointY, moveHexInX, moveHexInY, a);
                            hexPositions.Add(tuple, shape[a]);
                        }
                        else
                        {
                            shape[a] = NumericExtensions.calcHexagonPointLower(angle, angleRotation, radius, moveHexagons, startPointX, startPointY, moveHexInX, moveHexInY, a);
                            hexPositions.Add(tuple, shape[a]);
                        }
                    }
               }
               SolidBrush brush = CheckWhichBrush(moveHexInX, moveHexInY);
               if(brush.Color != Color.White)
               {
                   graphics.FillPolygon(brush, shape);
               }
               graphics.DrawPolygon(Pens.Black, shape);
            
        }

          private SolidBrush CheckWhichBrush(int moveHexInX, int moveHexInY)
          {
               SolidBrush brush = new SolidBrush(Color.White);
               if (hexagonMap.map[moveHexInX][moveHexInY].GetPlayer() == 1)
               {
                    brush = new SolidBrush(blueLogColor[hexagonMap.map[moveHexInX][moveHexInY].GetValue()]);
               }
               else if (hexagonMap.map[moveHexInX][moveHexInY].GetPlayer() == 2)
               {
                    brush = new SolidBrush(redLogColor[hexagonMap.map[moveHexInX][moveHexInY].GetValue()]);
               }

               return brush;
          }

          private void DrawHexValue(Graphics graphics, Font textFont, Brush textBrush,
               int i, int j, float radius, float textX, float textY, float x_0, float y_0, int angle,int moveHexagons, int fontSize, int angleRotation)
          {
                Tuple<int, int> tuple = new Tuple<int, int>(i, j);
                if (textPosition.ContainsKey(tuple))
                {
                    textX = textPosition[tuple];
                }
                else
                {
                    if (i < howManyHex.Value)
                    {
                        textX = x_0 + radius + NumericExtensions.GetXdistance(radius) * j - (NumericExtensions.GetXdistance(radius) / 2) * (i + 1) - 10;
                        textPosition.Add(tuple, textX);
                    }
                    else
                    {
                        textX = x_0 + radius * (float)Math.Cos((1 * NumericExtensions.ToRadians(angle)) + NumericExtensions.ToRadians(angleRotation))
                                    + NumericExtensions.GetXdistance(radius) * j - NumericExtensions.GetXdistance(radius) / 2 * i
                                    + NumericExtensions.GetXdistance(radius) * moveHexagons + NumericExtensions.GetXdistance(radius) / 2 - 10;
                        textPosition.Add(tuple, textX);
                    }
                }
                textY = y_0 + 2 * radius * (float)0.75 * i;
                graphics.DrawString(HexagonsMap.GetValueFromMap(hexagonMap.map, i, j).ToString(), textFont, textBrush, textX - fontSize / 2, textY - fontSize / 2);
                Points1.Text = "Points player 1: " + playerOne.GetScore().ToString();
                Points2.Text = "Points player 2: " + playerTwo.GetScore().ToString();
                Turn.Text = "Turn of player: " + whoseTurn.ToString();
          }


          #endregion

          private System.Windows.Forms.Label Points1;
          private System.Windows.Forms.Button startGame;
          private System.Windows.Forms.Button restart;
          private System.Windows.Forms.Panel drawBoard;
          private System.Windows.Forms.TrackBar howManyHex;
          private PictureBox turnIndicator;
          private Button upperLeft;
          private Button upperRight;
          private Button Right;
          private Button lowerRight;
          private Button lowerLeft;
          private Button Left;
          private Label Points2;
          private Label Turn;
     }
}

