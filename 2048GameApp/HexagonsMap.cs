﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
	class HexagonsMap
	{
		public Hex[][] map;
		public HexagonsMap(int mapSize)
		{
			ResizeMap(mapSize);
		}

		public void createHexObjects(Hex[][] map)
          {
			for (int i = 0; i < map.Length; i++)
			{
				for (int j = 0; j < map[i].Length; j++)
				{
					map[i][j] = new Hex(0, 0, 0);
				}
			}
		}

		public void printMap()
		{
			for (int i = 0; i < map.Length; i++)
			{
				for (int j = 0; j < map[i].Length; j++)
				{
					Console.Write(map[i][j].GetValue());
				}
				Console.WriteLine();
			}
		}

		public void ResizeMap(int newMapSize)
		{
			map = new Hex[newMapSize + newMapSize - 1][];
			int lenght = 0;
			for (int i = 0; i < map.Length; i++)
			{
				if (i < newMapSize)
				{
					map[i] = new Hex[newMapSize + lenght];
					lenght++;
				}
				else
				{
					lenght--;
					map[i] = new Hex[newMapSize + lenght - 1];
				}
			}
			createHexObjects(map);
               map = SetMapValue(map);
		}

		public static Hex[][] SetMapValue(Hex[][] map)
		{
			int liczba = 0;
			for (int i = 0; i < map.Length; i++)
			{
				for (int j = 0; j < map[i].Length; j++)
				{
					map[i][j].SetValue(liczba);
				}
			}
			return map;
		}

		public static int GetValueFromMap(Hex[][] map, int row , int column)
          {
			return map[row][column].GetValue();
          }
		public static void SetValueMap(Hex[][] map, int row, int column, int value)
		{
			map[row][column].SetValue(value);
		}

          public void SetRandomHexOnBoard(int player)
          {
               Random random = new Random();
               int indexi = random.Next(map.Length);
               int indexj = random.Next(map[indexi].Length);
               int howManyHexes = 0;
               int maxIteration = 0;
               for (int i = 0; i < map.Length; i++)
               {
                    for (int j = 0; j < map[i].Length; j++)
                    {
                         howManyHexes++;
                    }
               }
               while (map[indexi][indexj].GetValue() != 0 && maxIteration != howManyHexes)
               {
                    indexi = random.Next(map.Length);
                    indexj = random.Next(map[indexi].Length);
                    maxIteration++;
               }
               if (maxIteration != howManyHexes)
               {
                    int chanceOfFour = random.Next(100);
                    if(chanceOfFour>85)
                    {
                         map[indexi][indexj].SetValue(4);
                    }
                    else
                    {
                         map[indexi][indexj].SetValue(2);
                    }
                    map[indexi][indexj].SetPlayer(player);
                    if (player == 1)
                    {
                         map[indexi][indexj].SetColor(1);
                    }
                    if (player == 2)
                    {
                         map[indexi][indexj].SetColor(2);
                    }
               }
          }
	}

}
