﻿using System;
using System.Drawing;

public static class NumericExtensions
{
     public static double ToRadians(this double val)
     {
          return (Math.PI / 180) * val;
     }

     public static float GetXdistance(float radius)
     {
          return radius*(float)Math.Sqrt(3);
     }

     public static PointF calcHexagonPointUpper(int angle, int angleRotation, float radius, int moveHexagons, float startPointX, float startPointY, int moveHexInX, int moveHexInY, int a)
     {
          PointF point = new PointF(startPointX + radius * (float)Math.Cos((a * NumericExtensions.ToRadians(angle)) + NumericExtensions.ToRadians(angleRotation))
                         + NumericExtensions.GetXdistance(radius) * moveHexInY - NumericExtensions.GetXdistance(radius) / 2 * moveHexInX,
                         startPointY + radius * (float)Math.Sin((a * NumericExtensions.ToRadians(angle)) + NumericExtensions.ToRadians(angleRotation)) + 2 * radius * (float)0.75 * moveHexInX);

          return point;
     }

     public static PointF calcHexagonPointLower(int angle, int angleRotation, float radius, int moveHexagons, float startPointX, float startPointY, int moveHexInX, int moveHexInY, int a)
     {

          PointF point = new PointF(startPointX + radius * (float)Math.Cos((a * NumericExtensions.ToRadians(angle)) + NumericExtensions.ToRadians(angleRotation))
                         + NumericExtensions.GetXdistance(radius) * moveHexInY - NumericExtensions.GetXdistance(radius) / 2 * moveHexInX + NumericExtensions.GetXdistance(radius) * moveHexagons,
                         startPointY + radius * (float)Math.Sin((a * NumericExtensions.ToRadians(angle)) + NumericExtensions.ToRadians(angleRotation)) + 2 * radius * (float)0.75 * moveHexInX);

          return point;
     }
}
