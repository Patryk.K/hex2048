﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveHexLeft
     {
          public static void MoveLeft(Hex[][] map)
          {
               for (int i = 0; i < map.Length; i++)
               {
                    for (int j = 1; j < map[i].Length; j++)
                    {
                         int iterate = 1;
                         if (map[i][j].GetValue() != 0)
                         {
                              while (map[i][j - iterate].GetValue() == 0 && (j - iterate) >= 0)
                              {
                                   MoveBoard.ChangeBoard(map, i, j, 0, -iterate, 1, 0);
                                   if (iterate < map[i].Length)
                                   {
                                        iterate++;
                                   }
                                   if (j - iterate < 0)
                                   {
                                        break;
                                   }
                              }
                         }
                    }
               }
          }
     }
}
