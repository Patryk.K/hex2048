﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveHexUpperRight
     {
          public static void TopHalf(Hex[][] map, int x, int y, int iterate)
          {
               while (map[x - iterate][y].GetValue() == 0)
               {
                    MoveBoard.ChangeBoard(map, x, y, -iterate, 0, 0, 1);
                    if (x - iterate > 0)
                    {
                         iterate++;
                         if (map[x - iterate].Length == y)
                         {
                              break;
                         }
                    }
               }
          }

          public static void MoveUpperRight(Hex[][] map)
          {
               for (int i = 1; i < map.Length; i++)
               {
                    for (int j = 0; j < map[i].Length; j++)
                    {
                         int iterate = 1;
                         if (map[i][j].GetValue() != 0 && i < map[0].Length && i - iterate >= 0 && j < map[i - iterate].Length)
                         {
                              TopHalf(map, i, j, iterate);
                         }
                         BottomHalf(map, i, j, iterate);
                    }
               }
          }
          public static void BottomHalf(Hex[][] map, int i, int j, int iterate)
          {
               if (map[i][j].GetValue() != 0 && i > map[0].Length - 1 && j + iterate <= map[i - iterate].Length)
               {
                    while (map[i - iterate][j + iterate].GetValue() == 0)
                    {
                         MoveBoard.ChangeBoard(map, i, j, -iterate, iterate, -1, 1);
                         if (i - iterate >= map[0].Length - 1)
                         {
                              if (i - iterate <= map[0].Length - 1)
                              {
                                   break;
                              }
                              iterate++;
                         }
                    }
                    int nextJ = j + iterate;
                    iterate++;
                    if (map[i - iterate].Length > nextJ && i - iterate < map[0].Length - 1)
                    {
                         TopHalf(map, i, nextJ, iterate);
                    }
               }
          }
     }
}
