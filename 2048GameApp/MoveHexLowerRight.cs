﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveHexLowerRight
     {
          public static void TopHalf(Hex[][] map, int x, int y, int iterate)
          {
               while (map[x + iterate][y].GetValue() == 0)
               {
                    MoveBoard.ChangeBoard(map, x, y, iterate, 0, 0, -1);
                    if (x + iterate < map.Length)
                    {
                         iterate++;
                         if (map.Length == x + iterate)
                         {
                              break;
                         }
                         if (map[x + iterate].Length == y)
                         {
                              break;
                         }
                    }
               }
          }
          public static void MoveLowerRight(Hex[][] map)
          {
               for (int i = map.Length - 1; i >= 0; i--)
               {
                    for (int j = map[i].Length - 1; j >= 0; j--)
                    {
                         int iterate = 1;
                         if (i + iterate < map.Length)
                         {
                              if (map[i][j].GetValue() != 0 && i > map[0].Length - 2 && i + iterate <= map.Length && j < map[i + iterate].Length)
                              {
                                   TopHalf(map, i, j, iterate);
                              }
                         }
                         BottomHalf(map, i, j, iterate);
          }
               }
          }
          public static void BottomHalf(Hex[][] map, int i, int j, int iterate)
          {
               if (map[i][j].GetValue() != 0 && i < map[0].Length - 1 && j + iterate < map[i + iterate].Length)
               {
                    while (map[i + iterate][j + iterate].GetValue() == 0 && i + iterate <= map[0].Length - 1)
                    {
                         MoveBoard.ChangeBoard(map, i, j, iterate, iterate, -1, -1);
                         if (i + iterate < map[0].Length)
                         {
                              iterate++;
                              if (i + iterate == map[0].Length)
                              {
                                   break;
                              }
                              if (j > map[i + iterate].Length)
                              {
                                   break;
                              }
                         }
                    }
                    int nextJ = j + iterate - 1;
                    if (map[i + iterate].Length > nextJ && i + iterate > map[0].Length - 1)
                    {
                         TopHalf(map, i, nextJ, iterate);
                    }
               }
          }
     }
}
