﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048GameApp
{
     class MoveHexRight
     {
          public static void MoveRight(Hex[][] map)
          {
               for (int i = 0; i < map.Length; i++)
               {
                    for (int j = map[i].Length - 1; j >= 0; j--)
                    {
                         int iterate = 1;
                         if (map[i][j].GetValue() != 0 && j + iterate < map[i].Length)
                         {
                              while (map[i][j + iterate].GetValue() == 0 && j + iterate < map[i].Length)
                              {
                                   MoveBoard.ChangeBoard(map, i, j, 0, iterate, -1, 0);
                                   if (j + iterate < map[i].Length - 1)
                                   {
                                        iterate++;
                                   }
                              }
                         }
                    }
               }
          }
     }
}
